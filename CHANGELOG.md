## [v2.0.0](https://gitlab.com/bsara/gulp-fail/tree/v2.0.0) - 2020-01-03

* **[Dependency Updates]** Updated to latest version of `ansi-colors` and `through2` **(`through2` is no longer compatible with Node 0.10 & 0.12)**.
* Updated syntax to use newer keywords (`const`, `let`, etc.)
* Updated license year


## [v1.1.1](https://gitlab.com/bsara/gulp-fail/tree/v1.1.1) - 2018-08-07

* **[Dependency Updates]** Removed dependency of `gulp-util` and using individual dependencies now. Thanks [@victorhtc](https://github.com/victorhtc). ([Pull Request](https://github.com/bsara/gulp-fail/pull/2))
* Moved codebase to Gitlab.


## [v1.0.5](https://gitlab.com/bsara/gulp-fail/tree/v1.0.5) - 2016-03-29

* **[Bug Fix]** Add `this.emit('end')` to task to ensure that the task is always ended regardless of whether or not failure is supposed to occur.
* **[Bug Fix]** Fixed issues where no error was ever being created. Thanks [@bfrika](https://github.com/bfricka). ([Pull Request](https://github.com/bsara/gulp-fail/pull/1))
* Code cleanup
* Updated README/Documentation


## [v1.0.1](https://gitlab.com/bsara/gulp-fail/tree/v1.0.1) - 2015-07-29

* Initial Release
