/**
 * gulp-fail v2.0.1
 * Copyright (c) 2020 Brandon Sara (https://bsara.dev)
 * Licensed under the MIT License
 */

'use strict';


// Dependencies
// -----------------

const PluginError = require('plugin-error');
const colors = require('ansi-colors');
const through = require('through2');


// Constants
// -----------------

const PLUGIN_NAME = 'gulp-fail';



// Task Definition
// -----------------

function gulpFail(message, failAfterCompletion) {

  let shouldFail = false;
  let getMessage = message;



  if (typeof getMessage !== 'function') {
    getMessage = function() {
      return (message || "Task was forced to fail.");
    };
  }



  function getError() {
    return new PluginError(PLUGIN_NAME, colors.red(getMessage()), { showStack: false });
  };


  function checkFile(file, _e, cb) {
    if (failAfterCompletion !== true) {
      cb(getError());
      return;
    }

    shouldFail = true;

    cb(null, file);
  };


  function checkStream() {
    if (failAfterCompletion === true && shouldFail) {
      this.emit('error', getError());
      return;
    }
    this.emit('end');
  };


  function onError() {
    this.emit('end');
  };



  return through.obj(checkFile, checkStream)
                .on('error', onError);
}



// Module Exports
// -----------------

module.exports = gulpFail;
